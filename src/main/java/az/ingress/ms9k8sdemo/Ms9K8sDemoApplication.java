package az.ingress.ms9k8sdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms9K8sDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms9K8sDemoApplication.class, args);
    }

}
