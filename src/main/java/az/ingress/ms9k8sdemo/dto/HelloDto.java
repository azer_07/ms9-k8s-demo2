package az.ingress.ms9k8sdemo.dto;

import lombok.Data;

@Data
public class HelloDto {
    String message;
}
